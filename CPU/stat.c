#include <stdio.h>
#include <string.h>

#include "stat.h"
#include "parser_yacc.h"

static int __opcode_is_branch(int);
static int __opcode_is_load(int);
static int __opcode_is_store(int);

int counter [STAT_TYPES];
char log_buf [254];

void stat_init()
{
	memset(counter, 0, sizeof(counter));
}

void stat_add_by_opcode (int opcode) 
{
	counter [STAT_ALL] ++;
	if (__opcode_is_load (opcode))
		counter [STAT_LOAD] ++;
	else if (__opcode_is_store (opcode))
		counter [STAT_STORE] ++;
	else
		counter [STAT_OTHER] ++;
}

void stat_add_by_type (STAT_TYPE tp) 
{
	if (tp >= 0 && tp < STAT_TYPES)
		counter[tp]++;
}

int stat_get_count (STAT_TYPE tp) 
{
	if (tp >= 0 && tp < STAT_TYPES)
		return (counter[tp]);
	else
		return (-1);
}

char* stat_get_full_log () 
{
	sprintf (log_buf, "[Statistics]\t\
All:\t%d\t\
Load:\t%d\t\
Store:\t%d\t\
Jumped:\t%d\n", 
	counter[STAT_ALL], counter[STAT_LOAD], counter[STAT_STORE], counter[STAT_JUMP]);

	return (log_buf);
}

char* stat_get_short_log ()
{
	sprintf (log_buf, 
		"%d %d %d %d", counter[STAT_ALL], counter[STAT_LOAD], counter[STAT_STORE], counter[STAT_JUMP]);
	return (log_buf);
}

static int __opcode_is_branch (int opcode) 
{
	switch (opcode) {
    case Y_BC1F_OP:
    case Y_BC1FL_OP:
    case Y_BC1T_OP:
    case Y_BC1TL_OP:
    case Y_BC2F_OP:
    case Y_BC2FL_OP:
    case Y_BC2T_OP:
    case Y_BC2TL_OP:
    case Y_BEQ_OP:
    case Y_BEQL_OP:
    case Y_BEQZ_POP:
    case Y_BGE_POP:
    case Y_BGEU_POP:
    case Y_BGEZ_OP:
    case Y_BGEZAL_OP:
    case Y_BGEZALL_OP:
    case Y_BGEZL_OP:
    case Y_BGT_POP:
    case Y_BGTU_POP:
    case Y_BGTZ_OP:
    case Y_BGTZL_OP:
    case Y_BLE_POP:
    case Y_BLEU_POP:
    case Y_BLEZ_OP:
    case Y_BLEZL_OP:
    case Y_BLT_POP:
    case Y_BLTU_POP:
    case Y_BLTZ_OP:
    case Y_BLTZAL_OP:
    case Y_BLTZALL_OP:
    case Y_BLTZL_OP:
    case Y_BNE_OP:
    case Y_BNEL_OP:
    case Y_BNEZ_POP:
		return (1);
	case Y_J_OP:
	case Y_JAL_OP:
		return (1);
	default:
		return (0);
	}
}

static int __opcode_is_load (int opcode)
{
	switch (opcode) {
    case Y_LB_OP:
    case Y_LBU_OP:
    case Y_LH_OP:
    case Y_LHU_OP:
    case Y_LL_OP:
    case Y_LDC1_OP:
    case Y_LDC2_OP:
    case Y_LW_OP:
    case Y_LWC1_OP:
    case Y_LWC2_OP:
    case Y_LWL_OP:
    case Y_LWR_OP:
		return (1);
	default:
		return (0);
	}
}

static int __opcode_is_store (int opcode)
{
	switch (opcode) {
    case Y_SB_OP:
    case Y_SC_OP:
    case Y_SH_OP:
    case Y_SDC1_OP:
    case Y_SDC2_OP:
    case Y_SW_OP:
    case Y_SWC1_OP:
    case Y_SWC2_OP:
    case Y_SWL_OP:
    case Y_SWR_OP:
		return (1);
	default:
		return (0);
	}
}